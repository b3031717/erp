(function(){
    var sendBtn = document.querySelector("button.sendBtn");

    var handleClickSendBtn = function(){
        var _csrf = document.querySelector("input[name='_csrf']");
        var back_date = document.querySelector("input[name='back_date']");
        var datas = [`_csrf=${_csrf.value}`,`incomeDate=${back_date.value}`];

        if( back_date.value ){
            var req = new XMLHttpRequest();
            req.onreadystatechange = function(){
                if( req.readyState == XMLHttpRequest.DONE && req.status == 200 ){
                    var result = JSON.parse(req.responseText);
                    swal("儲存結果",result.message, "info");
                }
            }
            req.open("POST", "/plants/set-purchase-date", true);
            req.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
            req.send(datas.join("&"));
        }else{
            swal("錯誤","交貨日期尚未填入！", "error");
        }
        
    };
    sendBtn.addEventListener("click", handleClickSendBtn);
    
})();