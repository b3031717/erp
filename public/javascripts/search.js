(function(){
    var sendBtn = document.querySelector("button.sendBtn"),
        select = document.querySelector("select#page"),//分頁下拉
        startStatus = document.querySelector("input[name='order_status']"),
        nowLink = `${window.location.origin}${window.location.pathname}`,
        startSearch = window.location.search,
        startReplace = startSearch.replace(/\?/g, ""),
        startSplit = startReplace.split("&"),
        has_order_status = false;

    if( startReplace.search(`page=`) < 0 ){
        startSearch = `?page=1`;
    }
    //一開始檢查order_status 是否有勾選，並更f換連結search query
    for( var k=0; k< startSplit.length; k++ ){
        var thisSpilt = startSplit[k].split("=");
        if( thisSpilt[0] === "order_status" )has_order_status = true;
    }
    if( !has_order_status && startStatus.checked ){
        let status = ( startSearch )? `&order_status=1` : `?order_status=1`;
        window.history.pushState("", "", `${nowLink}${startSearch}${status}`);
    }
    //擷取目前連結中的query參數
    var getLinkParams = function(){
        var hash = window.location.search,
            hashReplace = hash.replace(/\?/g, ""),
            hashSpilt = hashReplace.split("="),
            req = new XMLHttpRequest(),
            _csrf = document.querySelector("input[name='_csrf']");

        hashReplace += `&_csrf=${_csrf.value}`;

        req.open("POST", `/plants/set-params`, true);
        req.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        req.send(hashReplace);
    }

    //分頁選單
    var handleSelectChange = function(){
        var _this = this,
            hash = window.location.search,
            replace = hash.replace(/\?/g, ""),
            spiltLink = replace.split("&"),
            newLink = "";

        for( var k=0; k< spiltLink.length; k++ ){
            var thisSpilt = spiltLink[k].split("=");
            newLink += ( thisSpilt[0] == "page" )?
                `page=${_this.value}` : `&${thisSpilt[0]}=${thisSpilt[1]}`;
        }
        window.location.href = `${nowLink}?${newLink}`;
    }
    //監聽搜尋送出
    var handleSendClick = function(){
        var dateStart = document.querySelector("input[name='from']");
        var dateEnd = document.querySelector("input[name='to']");
        var poNumber = document.querySelector("input[name='poNumber']");
        var order_status = document.querySelector("input[name='order_status']");
        var _csrf = document.querySelector("input[name='_csrf']");
        var loader = document.querySelector("div.loader");

        var req = new XMLHttpRequest();
        var datas = [`_csrf=${_csrf.value}`,`start_date=${dateStart.value}`,`end_date=${dateEnd.value}`,`poNo=${poNumber.value}`];
        var _this = this;

        if( order_status.checked ) datas.push(`order_status=${order_status.value}`);
        loader.style.display = "block";

        req.onreadystatechange = function(){
            if( req.readyState == XMLHttpRequest.DONE && req.status == 200 ){
                var result = JSON.parse(req.responseText);
                console.log(result);
                if( result.answer && result.data.length > 0 ){

                    var list = document.querySelector("tbody[id='list']"),
                        purchaseList = document.querySelector("tbody[id='PurchaseList']"),
                        pageCount = Math.ceil(result.dataCount / 15);

                    list.innerHTML = "";
                    purchaseList.innerHTML = "";
                    for( var k=0; k<result.data.length; k++ ){
                        var thisRow = result.data[k];
                        list.innerHTML +=`
                            <tr>
                                <td class="numeric">${thisRow.TA033}</td>
                                <td class="numeric">${thisRow.TA006}</td>
                                <td class="numeric">${thisRow.TA034}</td>
                                <td class="numeric">${thisRow.TA035}</td>
                                <td class="numeric">${thisRow.TA009}</td>
                                <td class="numeric">${thisRow.TA010}</td>
                                <td class="numeric">${thisRow.TA015}</td>
                                <td class="numeric">${thisRow.TA017}</td>
                                <td class="numeric">${thisRow.TA016}</td>
                                <td class="numeric">
                                    <a class=" btn default" href="/plants/detail/${thisRow.TA002}" >查看</a>
                                </td>
                            </tr>
                        `;
                    }
                    if(result.purchase.answer){
                        for( var j=0; j<result.purchase.dataList.length; j++ ){
                            var thisPur = result.purchase.dataList[j];
                            PurchaseList.innerHTML +=`
                                <tr>
                                    <td class="numeric">${thisPur.TD024}</td>
                                    <td class="numeric">${thisPur.TD002}</td>
                                    <td class="numeric">${thisPur.TD025}</td>
                                    <td class="numeric">${thisPur.TD004}</td>
                                    <td class="numeric">${thisPur.TD005}</td>
                                    <td class="numeric">${thisPur.TD006}</td>
                                    <td class="numeric">${thisPur.TD009}</td>
                                    <td class="numeric">${thisPur.TD012}</td>
                                </tr>
                            `;
                        }
                    }else{
                        PurchaseList.innerHTML = `
                            <tr>
                                <td class="numeric" colspan="8" align="center">沒有採購單</td>
                            </tr>
                        `;
                    }

                    var select = document.querySelector("select#page");

                    if( pageCount > 2 || pageCount == 2 ){
                        //如果原本沒有select才建立
                        if( !select ){
                            var div = document.querySelector("div#pagination");

                            div.innerHTML = `<div style="display:inline-block; font-size:16px;">分頁： </div>`;
                            div.innerHTML +=`<div style="display:inline-block" id="selectDiv"><select id="page"></select></div>`;
                            select = document.querySelector("select#page");

                        }else{
                            select.innerHTML = "";
                        }
                        for( var k=0; k<pageCount;k++ ){
                            select.innerHTML += `<option value="${k+1}">第${k+1}頁</option>`;
                        }
                        select.addEventListener("change", handleSelectChange);
                    }else if( select ){
                        select.innerHTML = "";
                    }
                    var hashString = `page=1&poNo=${poNumber.value}&start_date=${dateStart.value}&end_date=${dateEnd.value}`;
                    if( order_status.checked ) hashString += `&order_status=${order_status.value}`;

                    window.history.pushState("", "", `${nowLink}?${hashString}`);
                    getLinkParams();
                }else{
                    swal("搜尋結果",result.message, "info");
                }
                loader.style.display = "none";
            }
        };
        req.open("POST", `/plants/get-list`, true);
        req.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        req.send(datas.join("&"));
    };
    sendBtn.addEventListener("click", handleSendClick);
    getLinkParams();
    if( select ){
        select.addEventListener("change",handleSelectChange);
    }

})();
