(function(){
  	var firm = document.querySelector("div#firmData");
  	var sendBtn = document.querySelector("button.firmSendBtn");

  	sendBtn.addEventListener("click", function(){
		var store = firm.querySelector("select[name='store']");
		var loader = firm.querySelector("div.loader");
    	var inputs = firm.querySelectorAll("input");
		var data = [`store_id=${store.value}`];
		loader.style.display = "block";
		
		for(var k=0; k<inputs.length; k++){
			var thisInput = inputs[k];
			data.push( `${thisInput.name}=${thisInput.value}` );
		}
    
		if( store.value !=="not-select" ){
			var req = new XMLHttpRequest();
			req.onreadystatechange = function(){
				if( req.readyState == XMLHttpRequest.DONE && req.status == 200 ){
					var result = JSON.parse(req.responseText);
					var content = document.querySelector("div#firmContent");

					// console.log( result );
					
					content.innerHTML = `<div class="col-md-12" id="image" style="height:350px; width:90%"></div><div class="col-md-12" id="list"></div>`;
					//建立品號採購明細
					if(Object.getOwnPropertyNames(result.partRcord).length > 0){
						var purchaseArri = [];
						var allGet = [];
						var returnParsan = [];
						var listDiv = content.querySelector("div#list");//採購單明細
						listDiv.innerHTML = `<div id="purchase" style="margin-left: 15px;"></div>`;
						var purchase = listDiv.querySelector("div#purchase");
						
						//將所有篩選出來的採購單依照品號區分
						for( var item in result.partRcord ){
							var thisPart = result.partRcord[item];
							item = item.replace(/\s+/g, "");
							purchase.innerHTML += `<h3 class="col-md-12 modal-title">品號：${item} 品名：${thisPart.name} 規格：${thisPart.spec}</h3>`;
							purchase.innerHTML +=`<div class="col-md-12" id="no-${item}"></div>`;
							var thisPartList = document.querySelector(`div#no-${item}`);
							thisPartList.innerHTML += `<h4 class="modal-title">總購買量 ：${thisPart.totalBuy}  總到貨量：${thisPart.totalGet}  到貨率： ${thisPart.getParsan} % </h4>`;

							purchaseArri.push({ label: `品號：${item} 品名：${thisPart.name}`, y: thisPart.totalBuy }); //總購買量的長條圖資料
							allGet.push({ label: `品號：${item} 品名：${thisPart.name}`, y: thisPart.totalGet }); //總到貨量的長條圖資料
							returnParsan.push({ label: `品號：${item} 品名：${thisPart.name}`, y: thisPart.totalReturn }); //總退貨數量長條圖資料
							for( var poNumber in thisPart.Purchase ){
								var thisPurchase = thisPart.Purchase[poNumber];
								thisPartList.innerHTML += `<span class="col-md-6"> 訂單號： ${poNumber} 購買數：${thisPurchase.TD008} 到貨數：${thisPurchase.TD015} </span>`;
							}
						}
						var chart = new CanvasJS.Chart("image", {
							animationEnabled: true,
							title:{ text: `${result.firm.MA002} ` },
							toolTip: { shared: true },
							legend: { cursor:"pointer" },
							axisY: {
								title: "總購買量",
								titleFontColor: "#4F81BC",
								lineColor: "#4F81BC",
								labelFontColor: "#4F81BC",
								tickColor: "#4F81BC"
							},
							data: [{
								type: "column",
								name: "總購買數量",
								legendText: "總購買數量",
								showInLegend: true, 
								dataPoints: purchaseArri
							},
							{
								type: "column",	
								name: "總到貨",
								legendText: "總到貨量",
								// axisYType: "secondary",
								showInLegend: true,
								dataPoints: allGet
							},
							{
								type: "column",	
								name: "退貨數",
								legendText: "退貨數量",
								showInLegend: true,
								dataPoints: returnParsan
							}]
						});
						chart.render();
						if( document.querySelector("a.canvasjs-chart-credit") ){
							document.querySelector("a.canvasjs-chart-credit").style.display = "none";
						}
						$("div#purchase").accordion({ active : false });
					}else{
						swal(`搜尋「${result.firm.MA002}」結果`,`「${result.start_date} ~ ${result.end_date}」日期範圍並沒有該廠商之採購紀錄`, "info");
					}
					loader.style.display = "none";
				}
			}
			req.open("POST", "/system/firmscore/score", true);
			req.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
			req.send(data.join("&"));
		}else{
			swal("發生錯誤","必須選擇供應商！", "error");
		}
    
  	});
})();