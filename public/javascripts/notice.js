self.addEventListener('install', event => {
    console.log('[ServiceWorker] Install');
});
self.addEventListener('activate', event => {
    console.log( "is activate" );
    event.waitUntil(clients.claim());
});
self.addEventListener('fetch', event => {
    console.log('[ServiceWorker] fetch', event.request);
    var payload = event.data? event.data.text() : "";
    self.registration.showNotification('江門東駿線上聯絡單', {
        body: payload,
    });
});