(function(){
    //init html editor
    var $drag = $(`.drag-text`);
    var handleDrop = function(evt){

        if( evt.originalEvent.dataTransfer && evt.originalEvent.dataTransfer.files.length > 0 ){
            var files = evt.originalEvent.dataTransfer.files;
            var ajaxData = new FormData();
            for(var key in files){
                ajaxData.append("addfile[]", files[key], files[key].name);    
            }

            var req = new XMLHttpRequest();
            req.open(`POST`, `/editPost`, true);
            req.send(ajaxData);
        }
    }
    $drag.summernote({ height : "400px" });

    if( $("div.note-editor").size() > 0 ){
        $("div.note-editor").each(function(ind, el){
            $(this).on("drop", handleDrop);
        });
    }

})();