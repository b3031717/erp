(function(){
  var submitBtn = document.querySelector("a.submitBtn");

  function handleSubmitClick (){
    var postLink = '/system/prototype/update';
    var dataForm = document.querySelector("form#proStore");
    var ajaxData = new FormData(dataForm);
    var req = new XMLHttpRequest();
    req.onreadystatechange = function(){
      if( req.readyState == XMLHttpRequest.DONE && req.status == 200 ){
        var result = JSON.parse(req.responseText);
        console.log(result);
        if( result.answer ){
          window.location.reload();
        }else{
          swal("未更新",result.message, "error");
        }
      }
    }
    req.open("POST", postLink, true);
    req.send(ajaxData);
  }
  submitBtn.addEventListener("click", handleSubmitClick);
})();