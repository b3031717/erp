var { News, NewsChange, NewsRecord, NewsLog } = require("../models/news");
var dataPrimary = require("../dataTablePrimary");
const baseClass = require("./base");
const userClass = require("./user");
const { sendNotice } = require("./mosca");
const base = new baseClass();
const user = new userClass();

class news {
    async index(req, res){
        let result = await this.getList(req);
        let canEdit = await this.userPermission(req);
        let _token = req.csrfToken();

        res.render("news/index", { result, canEdit, _token});
    }
    create(req, res){
        let canEdit =  this.userPermission(req);
        if( canEdit ){
            res.render("news/create",{ 
                _token : req.csrfToken()
            });
        }else{
            res.redirect("/system/news");
        }

    }
    async store(req, res){
        let canEdit = this.userPermission(req);

        if( canEdit ){
            let {title, content} = req.body;
            let file = await base.uploadFile(req.files["addfile"]);
            
            if( title && req.files["addfile"] ){
                News.create({ title, file });
            }
        }
        res.redirect("/system/news");
    }
    async show(req, res){
        const { userInfo, userRole } = req.session;
        let {id} = req.params;
        let result = {
            _token : req.csrfToken(),
            canEdit : await this.userPermission(req)
        };
        
        result.News = await News.findOne({ where : {id} }).then(async news=>{ 

            var record = await news.getRecord();
            let saveSee = [""];

            //檢查有沒有看過
            for (let k=0; k<record.length; k++){
                record[k].Info = await user.getUserDepartment( record[k].user );
                saveSee[ record[k].user ] = true;
            }
            if( !saveSee[ userInfo.id ] ){
                NewsRecord
                    .create({
                        news_id : id,
                        user : parseInt(userInfo.id),
                        department : base.contentReplaceSpace(userInfo.department)
                    })
                    .then(async new_record=>{
                        new_record.Info = await user.getUserDepartment( userInfo.id );
                        record.push( new_record );
                    });
            }
            news.Record = record;
            return news 
        });
        //已經審核過的就不再修改
        result.is_check = ( !result.News.is_check )? true:false;
        res.render("news/show", { result });
    }
    async update(req, res){
        let canEdit = await this.userPermission(req);
        let { id, title, is_check } = req.body;
        if( canEdit ){
            is_check = ( is_check === "on" )? 1: 0;
        
            let base = new baseClass();
            let updateRow = {};
            var thisNews = await News.findOne({ where : { id } }).then(news=>{ return news });
            
            // let oldContent = await base.contentReplaceSpace(thisNews.content);
            // let newContent = await base.contentReplaceSpace(content);
            if( thisNews.is_check !== is_check && is_check === 1){
                updateRow.is_check = 1;
            }
            if(  req.files["addfile"] ){
                let newFile = await base.uploadFile( req.files["addfile"] );
                if( newFile ) updateRow.file = newFile;
            }
            if( title != thisNews.title ){
                updateRow.title = title;
            }
            // if( newContent != oldContent && !req.file ){
            //     updateRow.content = ( docContent )? docContent :content;
            // }
            if( Object.getOwnPropertyNames( updateRow ).length > 0 ){

                this.setChangeRecord( updateRow, thisNews, req.session.userInfo );
                await News.update(updateRow, {where:{ id }} );

                if( is_check ){
                    let userModel = user.thisUserModel();
                    let allEmails = await userModel
                        .findAll({ where : { is_active : 1 } })
                        .then(data=>{
                            let mails = [];
                            let ips = [];
                            for( let k=0; k<data.length; k++ ){
                                mails.push( data[k].email );
                                ips.push( data[k].ip );
                            }
                            return mails;
                        });
                    //先抓所有帳號信箱
                    if( allEmails.length > 0 ){
                        sendNotice(`訊息公告：${req.body.title}`, ips);
                        app.mailer.send('news/note',{
                            to : allEmails,
                            subject : '東駿系統信「訊息公告通知」',
                            userInfo : req.session.userInfo,
                            thisNews
                            },
                            (err)=>{ console.log( err ); }
                        );
                    }
                }

            }
        }
        res.redirect(`/system/news/show/${id}`);
    }
    download(req, res){
        let { id } = req.params;
        const { userInfo, userRole } = req.session;

        News
            .findOne({ where : { id } })
            .then(async  data=>{
                if( data && data.file ){
                    let fs = require("fs");
                    let downloadFile = `./uploads/${data.file}`;

                    fs.exists(downloadFile, exists =>{
                        if(exists){
                            let fileSubName = (data.file).split(".");

                            res.type(fileSubName[1]);
                            res.status(200).sendFile(`${data.file}`, { root : "./uploads/" });

                            //下載檔案就紀錄在「NewsLog」
                            NewsLog.create({
                                news_id : id,
                                user_id : parseInt(userInfo.id),
                                action : "openFile"
                            })
                            .catch(err=>console.log( err ));

                        }else{
                            res.send("檔案已不存在，請聯絡發單人員");
                        }
                    });
                }else{
                    res.send("查不到此筆公告紀錄，或此公告沒有附加檔案");
                }
            });
    }
    async delete(req, res){
        let result ={};
        let id = (req.body.id).split(",");

        if( id.length > 0 && this.userPermission(req) ){
            result = await
            News
            .update({ is_delete : 1 },{ where : { id : id } })
            .then(data=>{
                return {
                    answer : true,
                    message : "資料已刪除"
                }
            });
        }else{
            result.answer = false;
            result.message = "id 空白";
        }
        res.json(result);
    }
    async getList(req, limit){
        const {userInfo} = req.session;
        let result = {};
        let findSelect = {
            where : {},
            order : [['id',"DESC"]]
        };

        //如果有下最多幾筆時
        if( limit ) findSelect.limit = limit;
        if( !this.userPermission( req ) )findSelect.where.is_delete = 0; //如果沒有權限的，就只顯示沒刪除的
        if( !this.userPermission( req ) )findSelect.where.is_check = 1; //如果沒有權限的，就只顯示沒刪除的
        //取得所有公告訊息
        result.datas = await News.findAll( findSelect ).then(async news=>{
            for( let k=0; k <news.length; k++ ){
                let thisNew = news[k];
                news[k].Record = await thisNew.getRecord();
                news[k].Change = await thisNew.getChange();
                if( news[k].Record.length > 0 ){
                    for( let j=0; j<news[k].Record.length; j++ ){
                        let thisRecord = news[k].Record[j];
                        news[k].is_see = ( parseInt(thisRecord.user) === parseInt(userInfo.id) )? true: false;
                    }
                }
            }
            return news;
        });
        return result;
    }
    setChangeRecord(newData, oldData, user){
        //增加修改記錄
        for( let k in newData ){
            let saveChange = `{new : ${newData[k]}, old : ${oldData[k]} }`;

           NewsChange
                .create({
                    news_id : oldData.id,
                    c_department : base.contentReplaceSpace(user.department),
                    changer : user.account,
                    column : k,
                    record : saveChange
                });
        }
    }
    userPermission(req){
        let {userInfo} = req.session;
        //可編輯權限，必須要是「電腦室」的帳號 且 要有「可編輯」權限
        //可編輯權限包含「新增」「修改」「刪除」
        let canEdit = ( 
            userInfo.super_user === 1 || ( base.contentReplaceSpace(userInfo.department) === "D01" && userInfo.is_can_edit === 1 ) 
        )? true : false;

        return canEdit;
    }
}

module.exports = news;