const fs = require('fs');
const chokidar = require('chokidar');
const loadExcel = require('./loadExcel');

var watcher = chokidar.watch(`./dataFile`, {ignored: /(^|[\/\\])\../});

watcher.on('raw', async (event, path, details) =>{
    if( event === 'modified' ){
        let file_name = path.replace(/dataFile\//gi,'');
        let file_sub_name = file_name.split('.')[1];

        if( file_sub_name === 'xls' || file_sub_name === 'xlsx' ){
            await loadExcel(path);
            fs.unlinkSync(path);
        }
    }

});