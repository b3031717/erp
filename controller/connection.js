var sequelize = require("sequelize");
var diff = require("diff");
const dateformat = require('dateformat');
const { sendNotice } = require("./mosca");
// const urlSafeBase = require("urlsafe-base64");
// var webPush = require("web-push");
var { Connect, ConnectDepartment, ConnectRecord, ConnectChange, ConnectLog} = require("../models/connection");
var dataPrimary = require("../dataTablePrimary");
const baseClass = require("./base");
const userClass = require("./user");
const path = require("path");

var base = new baseClass();
var user = new userClass();
class connection {
    async index(req, res){
        let {userInfo} = req.session;
        let result = await this.getList(userInfo);
        sendNotice(`新聯絡單：測試發送`, ["192.168.1.201"]);
        result = await this.getConnectChanges( result );
        result.nearConnect = await this.getNearOnlineConnect(req);
        result._token = req.csrfToken();
        result.canEdit = this.userPermission(req);

        res.render("connection/index", { result });
    }
    async create(req, res){
        let result = {};
        let date = new Date();
        let month = ( date.getMonth().toString().length ===1 )? `0${date.getMonth()+1}` : `${date.getMonth()+1}`;
        let day = ( date.getDate().toString().length ===1 )? `0${date.getDate()}` : `${date.getDate()}`;

        result.errMessage = req.session.errMessage;
        result.allDeparment = await user.getAllDeparments();
        result._token = req.csrfToken();
        result.today = `${date.getFullYear()}-${month}-${day}`;

        req.session.errMessage = "";

        if( this.userPermission(req) ){
            res.render("connection/create", { result });
        }else{
            res.redirect("/system/connection");
        }
        
    }
    async store(req, res){
        let {userInfo, userRole} = req.session;
        let imgFileName = await base.uploadFile(req.files.addfile[0]);
        let redirect = "/system/connection";
        

        //檢查必填欄位是否為空
        let errMessage = await this.checkIsContentEmpty(req, ["title","file","remind_date","keyword"]);//檢查指定欄位是否為空
        if( errMessage ){
            req.session.errMessage = "有必填欄位空白";
            redirect = "/system/connection/create";
        }else if( req.files.addfile[0] && req.body.title && this.userPermission(req) ){ //有標題跟上傳檔案 且 有「可編輯」權限才新增資料
            //先儲存聯絡單本身
            let remind_date = (req.body.remind_date).replace(/\//gi,'-');
            let col = {
                owner : parseInt(req.session.userInfo.id),
                owner_department : base.contentReplaceSpace(req.session.userInfo.department),
                title : req.body.title,
                file : imgFileName,
                remind_date : remind_date,
                keyword : req.body.keyword
            };
            //如果沒有上傳檔案，就依照輸入欄位的內容
            if( !req.files.addfile[0] ) col.content = req.body.content;

            Connect
                .create(col)
                .then(newCon => {
                    //讀取doc並寫入content
                    // base.getDocxContent(req.file, Connect, newCon.null);
                    
                    if( req.body.to_department ){
                        const userModel = user.thisUserModel();
                        const toDepartment = req.body.to_department;
                        switch (typeof toDepartment) {
                            case "string":
                                ConnectDepartment.create({
                                    connection_id : newCon.null,
                                    department : toDepartment
                                }).catch(err=> { console.log( err ); });
                                break;
                            default:
                                for( let s=0; s<toDepartment.length; s++ ){
                                    ConnectDepartment.create({
                                            connection_id : newCon.null,
                                            department : toDepartment[s]
                                        }).catch(err=> { console.log( err ); });
                                }
                                break;
                        }
                        
                        

                        //發給對象部門通知信
                        //先收集對象部門的信箱

                        userModel.findAll({
                            where : { department : toDepartment },
                            attributes : ['email','ip']
                        })
                        .then(users =>{
                            let allEmails = [];
                            let allIPs = [];
                            for( let k in users ){
                                allEmails.push( users[k].email );
                                allIPs.push( users[k].ip );
                            }

                            if( allEmails.length > 0 ){
                                sendNotice(`新聯絡單：${req.body.title}`, allIPs);
                                app.mailer.send('connection/note',{
                                    to : allEmails,
                                    subject : '東駿系統信「聯絡單通知」',
                                    userInfo : req.session.userInfo,
                                    newCon
                                    },
                                    (err)=>{ console.log( err ); }
                                );
                            }
                        });
                    }
                });
        }else{
            req.session.errMessage = "標題與附加檔案為必填欄位！";
            redirect = "/system/connection/create";
        }
        res.redirect(redirect);
    }
    async show(req, res ){
        let { id, model } = req.params;
        let result = {
            canEdit : false,
            _token : req.csrfToken()
        };
        let redirect;
        if(!id) redirect = `/system/${model}`;//如果id欄位為空，就反回列表頁

        await
            Connect
                .findOne({ where : { id } })
                .then(async data=>{
                    //此筆聯絡單如果不是自己的或是，就只能觀看不能編輯
                    result.canEdit = ( parseInt(data.owner) === parseInt(req.session.userInfo.id) )? true : false;
                    //聯絡單內容
                    data.remind_date = dateformat(data.remind_date, "isoDate");
                    result.data = data;

                    //已勾選的單位
                    result.Departs = await ConnectDepartment
                        .findAll({ where : { connection_id : id } })
                        .then(depart=>{
                            let departs = [];
                            //為了不讓前臺搜尋存不存在時，因為陣列一就算有找到indexof也會是零，造成無法辨識是否存在所以先將[0]佔用
                            departs[0] = "";
                            for( let k=0; k<depart.length; k++ ){
                                departs.push( depart[k].department );
                            }
                            return departs;
                        });


                    //已觀看的帳號資訊
                    result.Record = await ConnectRecord
                        .findAll({ where : { connection_id : id } })
                        .then(async record=>{
                            let checkMySelf = [""];//檢查自己是否有看過，沒看過要新增進去
                            for( let k=0; k<record.length; k++ ){
                                //取得這個使用者詳細資料
                                record[k].Info = await user.getUserDepartment( record[k].user );
                                checkMySelf[record[k].user] = true;
                            }
                            if( !checkMySelf[req.session.userInfo.id] ){
                                ConnectRecord
                                    .create({
                                        connection_id : id,
                                        user : req.session.userInfo.id,
                                        department : base.contentReplaceSpace(req.session.userInfo.department)
                                    })
                                    .then(async newRecord=>{
                                        newRecord.Info = await user.getUserDepartment( req.session.userInfo.id );
                                        record.push( newRecord );
                                    });
                            }
                            return record;
                        });

                }).catch(err=>{ console.log( err ) });
        result.allDeparment = await user.getAllDeparments();
        res.render("connection/show", { result });
    }
    async update(req, res){
        let {userInfo, userRole} = req.session;
        let errMessage = await this.checkIsContentEmpty(req, ["title","remind_date","keyword"]);//檢查特定欄位是否為空

        if( errMessage ){
            req.session.errMessage = "有必填欄位空白";
        }else{
            Connect
                .findOne({ where : { id : req.body.id } })
                .then(async connect=>{
                    //先確定是否有權限修改
                    if( connect && userInfo.id == connect.owner ){
                        //讀取doc並更新content
                        base.getDocxContent(req.files.addfile[0], Connect, req.body.id);

                        let updateRow ={};
                        if(  req.files.addfile[0] ){
                            let newFile = await base.uploadFile( req.files.addfile[0] );
                            if( newFile ) updateRow.file = newFile;
                        }

                        //let to_department = ( req.body.to_department )? (req.body.to_department).join() : [];
                        // let connect_depart = await connect.getDepartment().then(data=>{
                        //     let row = [];
                        //     for( let k in data ){
                        //         row.push( data[k].department );
                        //     }
                        //     return row.join();
                        // });
                        // //確認是否有變更對象部門
                        // //先不處理變更聯絡部門，避免後續糾紛
                        // if( to_department != connect_depart && 0 ){
                        //     //編輯頁傳來的
                        //     for( let k=0; k<to_department.length; k++ ){
                        //         //狀況1, 不在原本的陣列中
                        //         //狀況2, 原本存在的被刪掉了
                        //     }
                        // }
                        for( let key in req.body ){
                            switch (key) {
                                // case "content":
                                //     let nowConent = await base.contentReplaceSpace( connect.content );//目前的內容
                                //     let newConent = await base.contentReplaceSpace( req.body.content );//送過來的內容

                                //     //沒有上傳檔案時，並且有輸入更新內容時
                                //     if( nowConent != newConent && !req.file ){
                                //         updateRow.content = req.body.content;
                                //     }
                                //     break;
                                case "remind_date":
                                    let remind_date = dateformat(req.body.remind_date, "isoDate");
                                    // let remind_date = (req.body.remind_date).replace(/\//gi,'-');
                                    if( connect.remind_date != remind_date ){
                                        updateRow.remind_date = remind_date;
                                    }
                                    break;
                                default:
                                    if( req.body[key] != connect[key] ){
                                        updateRow[key] = req.body[key];
                                    }
                                    break;
                            }
                        }

                        if( Object.getOwnPropertyNames( updateRow ).length > 0 ){
                            this.setChangeRecord(updateRow, connect, userInfo);
                            Connect.update(updateRow, { where : { id : req.body.id } });
                        }
                    }
                });
        }

        res.redirect(`/system/connection/show/${req.body.id}`);
    }
    //檔案下載
    download( req, res ){
        let { id } = req.params;
        let { userInfo, userRole } = req.session;

        Connect
            .findOne({ where : { id } })
            .then(async  data=>{
                if( data && data.file ){
                    let thisDepart = await data.getDepartment();
                    let checkUser = [""];
                    for( let k=0; k<thisDepart.length; k++ ){
                        checkUser.push( base.contentReplaceSpace(thisDepart[k].department) );
                    }
                    if( checkUser.indexOf( base.contentReplaceSpace( userInfo.department ) ) >0 || data.owner === userInfo.id ){
                        let fs = require("fs");
                        let downloadFile = `./uploads/${data.file}`;

                        fs.exists(downloadFile, exists =>{
                            if(exists){
                                let fileSubName = (data.file).split(".");

                                res.type(fileSubName[1]);
                                res.status(200).sendFile(`${data.file}`, { root : "./uploads/" });
                                //下載檔案就紀錄在「ConnectLog」
                                ConnectLog.create({
                                    connection_id : id,
                                    user_id : parseInt(req.session.userInfo.id),
                                    action : "openFile"
                                }).catch(err=>console.log( err ));

                            }else{
                                res.send("檔案已不存在，請聯絡發單人員");
                            }
                        });

                    }else{
                        res.send( "沒有權限可以下載此檔案" );
                    }
                }else{
                    res.send("查不到此筆聯絡單紀錄，或此聯絡單沒有附加檔案");
                }
            });
    }
    async delete(req, res){
        let result ={};
        let id = (req.body.id).split(",");

        if( id.length > 0 && this.userPermission(req) ){
            result = await
            Connect
            .update({ is_delete : 1 },{ where : { id : id } })
            .then(data=>{
                return {
                    answer : true,
                    message : "資料已刪除"
                }
            });
        }else{
            result.answer = false;
            result.message = "id 空白";
        }
        res.json(result);
    }
    async keyword(req, res){
        let { keyword } = req.body;
        let {userInfo, userRole} = req.session;
        let answer, message, data;

        if(!keyword){
            answer=fasle,message='關鍵字空白，無法搜尋';
        }else{
            data = await Connect
                .findAll({
                    attributes : ["id","title","owner"],
                    where : {
                        $or :  [
                            {keyword : { $like : `%${keyword},%` }},
                            {keyword : { $like : `%${keyword}%` }}
                        ],
                    },
                    order : [ ["id", "DESC"] ]
                })
                .then(async datas=>{
                    if( !datas ) answer = await false, message = await `關鍵字「${keyword}」沒有搜尋到相關資料`;
                    let data_can_see = [];
                    if( datas ){
                        answer = await true

                        for( let k in datas ){
                            let check = [""];
                            let thisRow = datas[k];
                            let departs = await thisRow.getDepartment() ;
                            for( let j in departs ){
                                check.push( departs[j].department );
                            }

                            //如果不是在聯絡單聯絡部門且不是自己發出去的，則不能看到
                            if( check.indexOf( base.contentReplaceSpace(userInfo.department) ) > 0 || userInfo.id == thisRow.owner){
                                data_can_see.push( thisRow );
                            }
                        }
                    };
                    return data_can_see;
                });
        }
        res.send({ answer, message, data });
    }
    async register(req, res){
        // console.log("======load connection register======");
        
        // const vapidKey = app.get("vapidKey");
        // let key = urlSafeBase.decode(vapidKey.publicKey);
        // console.log(key);
        // res.status(200).send(key);
    }
    async sendNoticfication(req, res){

        // const web_push_gen = app.get("web_push_gen");
        // const {endpoint, key, authSecret} = req.body;
        // const subscription = {
        //     endpoint, 
        //     keys : { p256dh: key, auth:authSecret } 
        // };
        // const payload = {
        //     title : "測試內容",
        //     message : "測試內內容拉",
        //     tag : "message-tag"
        // };
        // // const option = {
        // //     gcmAPIKey : app.get('priviateKey_web'),
        // //     vapidDetails : {
        // //         subject : "mailto:zh@summer-wind.info",
        // //         publicKey, privateKey
        // //     },
        // //     TTL : 0, 
        // // };
        // webPush.setGCMAPIKey("321376578601");
        // webPush.generateRequestDetails(subscription, "測試內容發送");
        // webPush.sendNotification(subscription, "測試內容發送")
        //     .then(()=>{ console.log("is send notice"); res.sendStatus(201) })
        //     .catch(err=>{ console.log(err), res.sendStatus(500) });

    };
    async getList(userInfo, limit=""){
        let connectID = [];//使用者部門聯絡單ids
        let con_result = [];//聯絡單列表
        let selfConn = [];//自己發出的聯絡單
        let selectObject = {
            where : { department : base.contentReplaceSpace(userInfo.department) } ,
            order : [["createdAt","DESC"]]
        };
        // if( limit !== '' ) selectObject.limit = limit;
        //先篩選出哪些是帳號所屬部門的聯絡單
        await ConnectDepartment
            .findAll(selectObject)
            .then( department =>{
            if( department.length !== 0 ){
                for ( let k =0; k< department.length; k++ ){
                    let thisRow = department[k];
                    connectID.push( thisRow.connection_id );
                }
            }
        });

        let ConnectSelect = {
            where : {
                $or : [{ id : connectID }, { owner : userInfo.id }],
            },
            order: [["id","DESC"]]
        };
        //如果不是超級使用者，那就不看到已經刪除的項目
        if( !userInfo.super_user ) ConnectSelect.where.is_delete = 0;

        await Connect
        .findAll(ConnectSelect)
        .then(async data=>{
            for( let k=0; k< data.length; k++ ){
                let thisRow = data[k];
                let thisRowRecord = await thisRow.getRecord().then(record =>{ return record });
                data[k].Record = thisRowRecord;
                data[k].hash_id = base.hashWebData((thisRow.id).toString());

                if( parseInt(thisRow.owner) === parseInt(userInfo.id) ){
                    selfConn.push( data[k] );
                }else{
                    //檢查有沒有看過
                    if( thisRowRecord ){
                        for( let s=0; s<thisRowRecord.length; s++ ){
                            let thisRecord = thisRowRecord[s];
                            if( parseInt(thisRecord.user) === parseInt(userInfo.id) ){
                                data[k].is_see = true;
                            }
                        }
                    }
                    con_result.push(data[k]);
                }
            }
        });
        return { datas : con_result, selfConn };
    }
    async getConnectChanges(result){
        for( let k=0; k<result.datas.length; k++ ){
            let thisRow = result.datas[k];
            result.datas[k].Change = await ConnectChange.findAll({ where : { connection_id : thisRow.id } }).then(data => { return data });
        }
        return result;
    }
    setChangeRecord(newData, oldData, user){
        //增加修改記錄
        for( let k in newData ){
            let saveChange = `{new : ${newData[k]}, old : ${oldData[k]} }`;
            if( k !== "_csrf" ){
                ConnectChange
                .create({
                    connection_id : oldData.id,
                    c_department : base.contentReplaceSpace(user.department),
                    changer : user.id,
                    column : k,
                    record : saveChange
                });
            }

        }
    }
    checkIsContentEmpty(req, checkCol){
        // let checkEmpty = ["title","content","remind_date","keyword"];
        let answer;
        for( var k=0; k<checkCol.length; k++ ){
            switch (checkCol[k]) {
                case `file`:
                    answer = ( req.file )? true : false;
                    break;
                default:
                    answer = ( req.body[checkCol[k]] === '' )? true : false;
                    break;
            }
            
        }
        return answer;
    }
    //抓取今天到往後三天需要提醒的聯絡單
    async getNearOnlineConnect(req){
        const {userInfo} = req.session;
        let today = new Date();
        let threeDay = new Date();
        threeDay.setDate( threeDay.getDate() + 3 ); //往後三天
        return await Connect
            .findAll({
                where : {
                    remind_date : { $between : [dateformat(today, "isoDate"), dateformat(threeDay, "isoDate")] }
                },
                order : [ ["remind_date", "ASC"] ]
            })
            .then(async data=>{
                let list = [];
                for( let k=0; k<data.length; k++ ){
                    let thisDeparts = await data[k].getDepartment();
                    let departList = [""];
                    for( let d=0; d<thisDeparts.length; d++ ){
                        departList.push(base.contentReplaceSpace(thisDeparts[d].department));
                    }
                    //有發給該單位才看到
                    if( departList.length > 0 && departList.indexOf( base.contentReplaceSpace(userInfo.department) ) > 0 ){
                        list.push( data[k] );
                    }
                }
                return list;
            });

    }
    userPermission(req){
        const { userInfo } = req.session;
        const canEditDepart = ["","CW","RND","GL"]; //可編輯之部門
        //可編輯權限，必須要是「開發部」的帳號 且 要有「可編輯」權限
        //可編輯權限包含「新增」「修改」「刪除」
        let canEdit = ( 
            userInfo.super_user === 1 || ( canEditDepart.indexOf( base.contentReplaceSpace( userInfo.department ) ) > 0 && userInfo.is_can_edit === 1 ) 
        )? true : false;

        return canEdit;
    }
}

module.exports = connection;
