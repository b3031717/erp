const xlsx = require('xlsx');
const Sequelize = require('sequelize');
const db = require('./db');


class ExcelData{

    loadData(fileName){
        return new Promise((resolve, reject)=>{
            try{
                let workbook = xlsx.readFile(fileName);
                let sheetList = workbook.SheetNames; //陣列資料，裡面有xls的每個sheet 名稱
                let data = [];

                for(let c=0; c<sheetList.length; c++){
                    let thisSheet = workbook.Sheets[sheetList[c]];// Sheet內容物件
                    let thisJson = xlsx.utils.sheet_to_json( thisSheet );//轉為json格式
                    //空的表格內容不處理
                    if( thisJson.length > 0 ){
                        //抓取欄位長度平均值
                        let colLength = {};

                        for( let v=0; v<thisJson.length; v++ ){
                            for( let k in thisJson[v] ){
                                //如果這筆資料長度比原本的長，取代原本的長度
                                //檢查json 中的key 是否存在
                                if( colLength.hasOwnProperty(k) ) {
                                    if( thisJson[v][k].length > colLength[ k ] ) colLength[ k ] = thisJson[v][k].length;
                                }else{
                                    colLength[ k ] = thisJson[v][k].length;
                                }
                                
                            }
                        }
                        data.push({
                            name : sheetList[c],
                            data : thisJson,
                            col_lenght : colLength
                        });
                    }
                }

                resolve(data);
            }catch(err){
                reject(err);
            }
        });
    }
    async setSheetTable(sheet){
        try{
            for( let s=0; s<sheet.length ; s++ ){
                let thisSheetRow = sheet[s];
                let cols = Object.keys( thisSheetRow.data[0] );//收集欄位

                //建立orm 欄位type object
                let colDefine = await this.getCols(cols);
                let model = new db(thisSheetRow.name, colDefine);
                model.checkTableExist(thisSheetRow);

            }
            return true;
        }catch(err){
            throw err;
        }
    }
    getCols(cols){
        let colDefine = {};
        for( let c=0; c <cols.length; c++ ){
            let thisRow = cols[c];
            
            colDefine[ cols[c] ] = { type : Sequelize.STRING };
        }
        return colDefine;
    }

}

//非同步處理Excel裡面的資料
module.exports = async (file)=>{
    
    let excel = new ExcelData();
    //讀取文件中的資料並轉為json格式
    let data =  await excel.loadData(file);
    //產生文件中的資料表與欄位
    return await excel.setSheetTable(data);
    
}