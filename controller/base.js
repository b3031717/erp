const word = require("textract");
const activeMethod = require("../activeMethod");
var crypto = require('crypto-js');
var fs = require("fs");

class base {
    unHashPostData(hash){
        let unHash =crypto.AES.decrypt(hash.toString(), app.get('priviateKey'));
        return JSON.parse( unHash.toString( crypto.enc.Utf8 ) );
    }
    hashResData(data){
        return crypto.AES.encrypt( JSON.stringify(data), app.get('priviateKey') ).toString();
    }
    hashWebData(data){
        return crypto.AES.encrypt( data, app.get('priviateKey_web') ).toString();
    }
    unHashWebData(data){
        let unHash =crypto.AES.decrypt(data.toString(), app.get('priviateKey_web'));
        return  unHash.toString( crypto.enc.Utf8 );
    }
    async uploadFile(file, uploadPath=""){
        if( !file ) return ""; 
        
        let {originalname, filename, path} = file;
        let subFileName = originalname.split('.');
        let confrimFileType = ["","jpg","jpeg","png","bmp","pdf"];
        let upload_path = ( uploadPath )? `uploads/${uploadPath}/${originalname}` : `uploads/${originalname}`;
        
        if( confrimFileType.indexOf( subFileName[1] ) >0 ){
            //改變原本沒有副檔名的檔案, 
            fs.exists(`./${upload_path}`,function(isExists){
                if( !isExists ){
                    fs.rename(`${path}`, upload_path, (err)=>{
                        console.log(err);
                    });
                    //如果不是在uploads,就把暫存刪掉
                    if( uploadPath ){
                        fs.unlink(`./${path}`, (err)=>{
                            console.error( err );
                        });
                    }
                }
            });
            
            return originalname;
        }else{
            return false;
        }

    }
    getDocxContent(file, Model, id){
        if( !file ) return ""
        let {originalname, filename, path} = file;
        let spiltFileName = originalname.split(".");

        if( spiltFileName[1] === "docx" || spiltFileName[1] === "doc" ){
            return word.fromFileWithPath(`uploads/${originalname}`, function( error, content ) {
                try {
                    Model.update({ content }, { where : { id } });                    
                } catch (err) {
                    console.log(err)
                }

            });
        }

    }
    contentReplaceSpace(content){
        if( content ){
            return content.replace(/(?:\r\n|\r|\n)/g, '').replace(/ /g, '');
        }
        
    }
    checkRouteMehtod(req, res, next){
        //審核包含允許ＧＥＴ或ＰＯＳＴ
        let classActiveMethod = activeMethod[req.params.model][req.method];
        
        if( classActiveMethod.indexOf( req.params.method ) >0 ){
            next();
        }else{
            var err = new Error('此路徑並不被允許使用');
            err.status = 404;
            next(err);
        }
    }
    getNowDate(){
        let now = new Date();
        let month =( (now.getMonth()+1).length == 1 )? `0${now.getMonth()+1}`: now.getMonth()+1;
        let day =( (now.getDate().toString()).length == 1 )? `0${now.getDate()}`: now.getDate();
        return `${now.getFullYear()}${month}${day}`;
    }
}


module.exports = base;