module.exports = [
    {
        title : "公告事項",
        link : "/system/news",
        icon : "icon-docs",
        uuid : "news".toString('base64')
    },
    {
        title : "聯絡單",
        link : "/system/connection",
        icon : "icon-envelope-open",
        uuid : "connections".toString('base64')
    },
    {
        title : "工單及採購單",
        link : "/system/plant",
        icon : "icon-hourglass",
        uuid : "plant".toString('base64')
    },
    // {
    //     title : "供應商評比",
    //     link : "/system/firmscore",
    //     icon : "icon-book-open",
    //     uuid : "plant".toString('base64')
    // }
]
