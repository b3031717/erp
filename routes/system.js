var appRouter = express.Router();
var fs = require("fs");
var base = require("../controller/base");
var nowCLASS = {};
var baseClass = new base();
var type = fileUpload.fields([{ name: 'addfile', maxCount: 1 }, { name: 'photoes', maxCount: 8 }]);

appRouter.get("/", async (req, res)=>{
    var newsClass = require("../controller/news");
    var connectClass = require("../controller/connection");
    var connect = new connectClass();
    var news = new newsClass();
    
    let nearConnect = await connect.getNearOnlineConnect(req);

    let newsAll = await news.getList(req, 10);
    let con_result = await connect.getList(req.session.userInfo, 10);

    con_result = await connect.getConnectChanges( con_result );
    res.render("index",{
        connect : con_result,
        News : newsAll.datas,
        nearConnect
    });
});

appRouter.use("/:model", async(req, res, next)=>{
    let model = req.params.model;
    let CLASS = require(`../controller/${model}`);
    const { userInfo } = req.session;
    
    if( model === "accounts" ){
        if( !userInfo.super_user ) res.redirect("/system");
    }
    nowCLASS = new CLASS();

    next();    
});
appRouter.use("/:model/:method", ( req, res, next )=>{ baseClass.checkRouteMehtod(req, res, next) });
appRouter.use("/:model/:method/:id", ( req, res, next )=>{ baseClass.checkRouteMehtod(req, res, next) });


appRouter.get("/:model", csrfProtection, (req, res)=>{ nowCLASS.index(req, res); });
appRouter.get("/:model/:method", csrfProtection, (req, res)=>{ nowCLASS[ req.params.method ](req, res) });
appRouter.get("/:mode/:method/:id", csrfProtection, (req, res)=>{ nowCLASS[ req.params.method ](req, res) });
appRouter.post("/:model/:method", type, csrfProtection, (req, res)=>{ nowCLASS[req.params.method](req, res) });

module.exports = appRouter;