// 2017.9.20 因為一班文員不一定可以連接wifi,所以開放給外部連接
// app.use(["/","/system","/manual","/plants"],
// 	(req, res, next)=>{
// 		if( (/127.0.0.1/).test(req.ip) || (/192.168.[0-9]./).test(req.ip) ){
// 			next();
// 		}else{
// 			var err = new Error('只允許內部使用');
// 			err.status = 404;
// 			next(err);
// 		}
		
// 	}
// );
app.use('/',  require('./home'));
app.use('/api', require('./api'));
app.use("/manual", require("./manual"));

app.use(["/system","/plants"], (req, res, next)=>{
	app.locals.baseUrl = "http://"+req.headers.host;

	if( !req.session.userInfo ){
		res.redirect('/');
	}else{
		res.locals.menuList = app.get('appList');
		res.locals.loginUser = req.session.userInfo;
		next();
	}
});

app.use("/plants", require("./plants"));
app.use('/system', require('./system'));