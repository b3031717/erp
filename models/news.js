let sequelize = require("sequelize");
let _seq = app.get('web_db');
let dataPrimary = require("../dataTablePrimary");

const News = _seq.define(
    "News", dataPrimary.News.column,
    { tableName : "news", primaryKey : true }
);
const NewsRecord = _seq.define(
    "NewsRecord", dataPrimary.NewsRecord.column,
    { tableName : "news_records", primaryKey : true }
);
const NewsChange = _seq.define(
    "NewsChange", dataPrimary.NewsChange.column,
    { tableName : "news_changes", primaryKey : true }
);
const NewsLog = _seq.define(
    "NewsLog", dataPrimary.NewsLog.column,
    { tableName : "news_logs", primaryKey : true }
);

News.hasMany(NewsRecord, { foreignKey : "news_id", as : "Record" });
News.hasMany(NewsChange, { foreignKey : "news_id", as : "Change" });
News.hasMany(NewsLog, { foreignKey : "news_id", as : "Log" });

NewsRecord.belongsTo(News, { foreignKey : "news_id", as : "News" });
NewsChange.belongsTo(News, { foreignKey : "news_id", as : "News" });
NewsLog.belongsTo(News, { foreignKey : "news_id", as : "News" });

exports.News = News;
exports.NewsRecord = NewsRecord;
exports.NewsChange = NewsChange;
exports.NewsLog = NewsLog;