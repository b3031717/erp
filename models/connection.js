let sequelize = require("sequelize");
let _seq = app.get('web_db');
let dataPrimary = require("../dataTablePrimary");

const Connect = _seq.define(
    "Conntion", dataPrimary.Conntion.column,
    { tableName : "connections", primaryKey : true }
);

const ConnectDepartment = _seq.define(
    "ConntionDepartment", dataPrimary.ConntionDepartment.column,
    { tableName : "connection_departments" }
);

const ConnectRecord = _seq.define(
    "ConnectRecord", dataPrimary.ConnectRecord.column,
    { tableName : "connection_records" }
);
const ConnectChange = _seq.define(
    "ConnectRecord", dataPrimary.ConnectChange.column,
    { tableName : "connection_changes" }
);
const ConnectLog = _seq.define(
    "ConnectLog", dataPrimary.ConnectLog.column,
    { tableName : "connection_logs" }
);

Connect.hasMany(ConnectDepartment, { foreignKey : "connection_id", as : "Department" });
Connect.hasMany(ConnectRecord, { foreignKey : "connection_id", as : "Record" });
Connect.hasMany(ConnectChange, { foreignKey : "connection_id", as : "Change" });
Connect.hasMany(ConnectLog, { foreignKey : "connection_id", as : "Log" });

ConnectDepartment.belongsTo(Connect, { foreignKey : "connection_id", as : "Connect" });
ConnectRecord.belongsTo(Connect, { foreignKey : "connection_id", as : "Connect" });
ConnectChange.belongsTo(Connect, { foreignKey : "connection_id", as : "Connect" });
ConnectLog.belongsTo(Connect, { foreignKey : "connection_id", as : "Connect" });

exports.Connect = Connect;
exports.ConnectDepartment = ConnectDepartment;
exports.ConnectRecord = ConnectRecord;
exports.ConnectChange = ConnectChange;
exports.ConnectLog = ConnectLog;